package bouncingshape;

public class VertexBufferObject {

	public Vector3D[] vertexBuffer;

	public int[]  indexBuffer;

	public int vertexCount;

	public int triangleCount;

	public VertexBufferObject(Vector3D[] vertexBuffer, int[]  indexBuffer) {
		this.vertexBuffer = vertexBuffer;
		this.indexBuffer = indexBuffer;
		this.vertexCount = vertexBuffer.length;
		this.triangleCount = indexBuffer.length/3;
	}

}
