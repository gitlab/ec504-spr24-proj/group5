package bouncingshape;

public class Rasterizer {
    // Set the resolution of the screen
    public static int screen_w = MainThread.screen_w;
    public static int screen_h = MainThread.screen_h;
    public static int half_screen_w = MainThread.half_screen_w;
    public static int half_screen_h = MainThread.half_screen_h;
    public static int screenSize = screen_w * screen_h;

    // Pixel group of the screen
    public static int[] screen = MainThread.screen;

    // Depth buffering of the screen
    public static float[] zBuffer = MainThread.zBuffer;

    // The distance from the origin of the perspective to the screen
    public static int screenDistance = 815;

    // Untransformed triangle vertices
    public static Vector3D[] triangleVertices;

    // Transformed triangle vertices
    public static Vector3D[] updatedVertices;

    // The number of vertices in a triangle
    public static int verticesCount = 3;

    // The 2D coordinates of the vertex projection after triangle transformation on the screen
    public static float[][] vertices2D = new float[4][2];

    // Two arrays used to scan the filled area of a triangle, with two values in each row, representing the x-coordinates of the starting and ending points of the line drawing
    public static int[] xLeft = new int[screen_h],
            xRight = new int[screen_h];

    // Two arrays used to scan the depth of triangles, with two values in each row, representing the z-values of the starting and ending points of the line tracing
    public static float[] zLeft = new float[screen_h],
            zRight = new float[screen_h];

    // Used to record the depth value of triangle vertices
    public static float[] vertexDepth = new float[4];

    // The highest and lowest positions of the triangular scanning line
    public static int scanUpperPosition, scanLowerPosition;

    // The highest and lowest, leftmost and rightmost positions of a triangle
    public static float leftMostPosition, rightMostPosition, upperMostPosition, lowerMostPosition;

    // The color of triangles
    public static int triangleColor;

    // The type of triangle
    public static int renderType;

    // The distance between the Z clipping plane and the origin of the viewing angle
    public static float nearClipDistance = 0.01f;

    // Determine whether the triangle is cropped by the Z clipping plane
    public static boolean needToBeClipped;

    // Translation transformation of the coordinate system of the object where the triangle is located
    public static Vector3D localTranslation;

    // Rotation transformation of the coordinate system of the object where the triangle is located
    public static int localRotationX, localRotationY, localRotationZ;

    // Vector class variables for auxiliary rendering calculations
    public static Vector3D surfaceNormal, edge1, edge2, tempVector1, clippedVertices[];

    // Determine whether the triangle is tangent to the left and right sides of the screen
    public static boolean isClippingRightOrLeft;

    // Vertex buffer object
    public static VertexBufferObject VBO;

    // Container used to store transformed vertices
    public static Vector3D[] updatedVertexBuffer;

    // The maximum number of vertices that the renderer can handle at once
    public static int maxNumberOfVertex = 10002144;


    public static void init(){
        updatedVertices = new Vector3D[]{
                new Vector3D(0,0,0),
                new Vector3D(0,0,0),
                new Vector3D(0,0,0),
        };

        // Translation transformation of the initial coordinate system
        localTranslation = new Vector3D(0,0,0);

        // Initialize temporary defined vector variables for auxiliary rendering
        surfaceNormal = new Vector3D(0,0,0);
        edge1 = new Vector3D(0,0,0);
        edge2 = new Vector3D(0,0,0);
        tempVector1 = new Vector3D(0,0,0);
        clippedVertices = new Vector3D[]{
                new Vector3D(0,0,0),
                new Vector3D(0,0,0),
                new Vector3D(0,0,0),
                new Vector3D(0,0,0)
        };

        // Initialize the container used to store transformed vertices
        updatedVertexBuffer = new Vector3D[maxNumberOfVertex];
        for(int i = 0; i < maxNumberOfVertex; i ++) {
            updatedVertexBuffer[i] = new Vector3D(0,0,0);
        }
    }

    // Entry point for the raster renderer
    public static void rasterize(){
        // Transform the vertices of the triangle
        transformVertices();

        // Render the triangles in the vertex buffer object
        for(int i = 0; i < VBO.triangleCount; i++) {

            // Build triangles using the index buffer
            updatedVertices[0] = updatedVertexBuffer[VBO.indexBuffer[i*3+2]];
            updatedVertices[1] = updatedVertexBuffer[VBO.indexBuffer[i*3+1]];
            updatedVertices[2] = updatedVertexBuffer[VBO.indexBuffer[i*3]];

            // Test if the triangle should be rendered
            if(testHidden())
                continue;

            MainThread.triangleCount++;

            // Clip the part of the triangle in the Z near plane
            clipZNearPlane();

            // Convert the triangle to scan lines
            scanTriangle();

            // Color the pixels of the triangle
            renderTriangle();
        }


    }

    // Transform the vertices of the triangle
    public static void transformVertices(){

        // Get the trigonometric function values needed for the transformation angles from the pre-calculated trigonometric tables
        float local_sinX = LookupTables.sin[localRotationX];
        float local_cosX = LookupTables.cos[localRotationX];
        float local_sinY = LookupTables.sin[localRotationY];
        float local_cosY = LookupTables.cos[localRotationY];
        float local_sinZ = LookupTables.sin[localRotationZ];
        float local_cosZ = LookupTables.cos[localRotationZ];

        float global_sinY = LookupTables.sin[360- Camera.Y_angle];
        float global_cosY = LookupTables.cos[360-Camera.Y_angle];
        float global_sinX = LookupTables.sin[360-Camera.X_angle];
        float global_cosX = LookupTables.cos[360-Camera.X_angle];

        for(int i = 0; i < VBO.vertexCount; i++) {
            updatedVertexBuffer[i].set(VBO.vertexBuffer[i]);

            // Transform the vertices in the vertex buffer according to the coordinate system of the object itself
            updatedVertexBuffer[i].rotate_X(local_sinX, local_cosX);
            updatedVertexBuffer[i].rotate_Y(local_sinY, local_cosY);
            updatedVertexBuffer[i].rotate_Z(local_sinZ, local_cosZ);
            updatedVertexBuffer[i].add(localTranslation);

            // Transform the vertices in the vertex buffer according to the inverse direction of the viewpoint
            updatedVertexBuffer[i].subtract(Camera.position);
            updatedVertexBuffer[i].rotate_Y(global_sinY, global_cosY);
            updatedVertexBuffer[i].rotate_X(global_sinX, global_cosX);
        }
    }

    // Hidden surface test
    public static boolean testHidden() {
        // Test 1: If all vertices of the triangle are behind the Z clipping plane, then the triangle is considered hidden
        boolean allBehindClippingPlane = true;
        for(int i = 0; i < 3; i++) {
            if(updatedVertices[i].z >= nearClipDistance) {
                allBehindClippingPlane = false;
                break;
            }
        }
        if(allBehindClippingPlane)
            return true;

        // Test 2: Calculate the surface normal vector of the triangle and check if it is facing the viewpoint
        edge1.set(updatedVertices[1]);
        edge1.subtract(updatedVertices[0]);
        edge2.set(updatedVertices[2]);
        edge2.subtract(updatedVertices[0]);
        surfaceNormal.cross(edge1, edge2);
        float dotProduct  = surfaceNormal.dot(updatedVertices[0]);
        // If it is not facing the viewpoint, then the triangle is considered hidden
        if(dotProduct >= 0)
            return true;


        // Test 3: Determine if the triangle is outside the screen
        for(int j = 0; j < 3; j++) {
            // Use the projection formula to calculate the 2D coordinates of the vertices on the screen
            vertices2D[j][0] = half_screen_w + updatedVertices[j].x*screenDistance/updatedVertices[j].z;
            vertices2D[j][1] = half_screen_h - updatedVertices[j].y*screenDistance/updatedVertices[j].z;

            // Get the depth value of the vertices
            vertexDepth[j] = 1f/updatedVertices[j].z;
        }

        leftMostPosition = screen_w;
        rightMostPosition = -1;
        upperMostPosition = screen_h;
        lowerMostPosition = -1;
        for(int i = 0; i < 3; i++) {
            // Calculate the leftmost and rightmost positions of this triangle
            if(vertices2D[i][0] <= leftMostPosition)
                leftMostPosition = vertices2D[i][0];
            if(vertices2D[i][0] >= rightMostPosition)
                rightMostPosition = vertices2D[i][0];

            // Calculate the uppermost and lowermost positions of this triangle
            if(vertices2D[i][1] <= upperMostPosition)
                upperMostPosition = vertices2D[i][1];
            if(vertices2D[i][1] >= lowerMostPosition)
                lowerMostPosition = vertices2D[i][1];
        }

        // If the leftmost, rightmost, uppermost, or lowermost positions of this triangle have not been reassigned, then this triangle is definitely outside the screen range, so it will not be rendered.
        if(leftMostPosition == screen_w ||  rightMostPosition == -1 || upperMostPosition == screen_h || lowerMostPosition == -1) {
            return true;
        }

        // Determine if the triangle is tangent to the left and right sides of the screen
        isClippingRightOrLeft = false;
        if(leftMostPosition < 0 || rightMostPosition >= screen_w)
            isClippingRightOrLeft = true;


        return false;
    }

    // Clip the part of the triangle behind the Z clipping plane
    public static void clipZNearPlane() {
        // Generally, the number of vertices of a triangle is 3, which may become 4 after clipping.
        verticesCount = 0;

        needToBeClipped = false;

        for(int i = 0; i < 3; i++){
            // If the vertex is in front of the clipping plane, do not make any changes
            if(updatedVertices[i].z >= nearClipDistance){
                clippedVertices[verticesCount].set(updatedVertices[i]);
                verticesCount++;
            }
            // If the vertex is behind the clipping plane, the triangle needs to be clipped
            else{
                needToBeClipped = true;

                // Find the previous vertex (i.e., the vertex adjacent to the current vertex on the edge of the triangle)
                int index = (i+3 - 1)%3;
                if(updatedVertices[index].z >= nearClipDistance){
                    // If the previous vertex is in front of the clipping plane, find the intersection point of the line segment between the current vertex and the previous vertex on the clipping plane
                    approximatePoint(verticesCount, updatedVertices[i], updatedVertices[index]);
                    verticesCount++;
                }
                // Find the next vertex (i.e., another vertex adjacent to the current vertex on the edge of the triangle)
                index = (i+1)%3;
                if(updatedVertices[index].z >= nearClipDistance){
                    // If the next vertex is in front of the clipping plane, find the intersection point of the line segment between the current vertex and the next vertex on the clipping plane
                    approximatePoint(verticesCount, updatedVertices[i], updatedVertices[index]);
                    verticesCount++;
                }
            }
        }

        // If the triangle is clipped by the clipping plane, recalculate the parameters related to the vertices
        if(needToBeClipped) {
            for(int i = 0; i < verticesCount; i++) {
                // Use the projection formula to calculate the 2D coordinates of the vertices on the screen
                vertices2D[i][0] = half_screen_w + clippedVertices[i].x*screenDistance/clippedVertices[i].z;
                vertices2D[i][1] = half_screen_h - clippedVertices[i].y*screenDistance/clippedVertices[i].z;

                // Get the depth value of the vertices
                vertexDepth[i] = 1f/clippedVertices[i].z;
            }
        }
    }

    // Find the intersection point of the line segment between two points on the clipping plane
    public static void approximatePoint(int index, Vector3D behindPoint, Vector3D frontPoint){

        // The ratio of the position of the intersection point on the line segment
        tempVector1.set(frontPoint.x - behindPoint.x, frontPoint.y - behindPoint.y, frontPoint.z - behindPoint.z);
        float ratio = (frontPoint.z- nearClipDistance)/tempVector1.z;

        // The direction vector of the line segment multiplied by this ratio gives the position of the intersection point
        tempVector1.scale(ratio);
        clippedVertices[index].set(frontPoint.x, frontPoint.y, frontPoint.z);
        clippedVertices[index].subtract(tempVector1);
    }


    // Convert the triangle to scan lines
    public static void scanTriangle() {
        // Initialize the highest, lowest, leftmost, and rightmost positions of the scan lines
        scanUpperPosition = screen_h;
        scanLowerPosition = -1;

        // Scan each edge of the triangle
        for(int i = 0; i < verticesCount; i++){
            float[] vertex1 = vertices2D[i];    // Get the first vertex
            float[] vertex2;

            float depth1 = vertexDepth[i];  // Get the depth value of the first vertex
            float depth2;

            if(i == verticesCount -1 ){    // If it is the last vertex
                vertex2 = vertices2D[0];   // Then the second point is the first vertex
                depth2 = vertexDepth[0];
            }else{
                vertex2 = vertices2D[i+1];   // Otherwise, the second vertex is the next vertex
                depth2 = vertexDepth[i+1];
            }

            boolean downwards = true;   // Default to a descending edge

            if (vertex1[1]> vertex2[1]) {    // If the first vertex is lower than the second vertex
                downwards = false;           // Then it is an ascending edge
                float[] temp = vertex1;      // Swap the two points, so the scan always goes from top to bottom
                vertex1 = vertex2;
                vertex2 = temp;

                float tempDepth = depth1;   // Swap the depth values of the two points
                depth1 = depth2;
                depth2 = tempDepth;
            }

            // Ignore horizontal edges
            float dy = vertex2[1] - vertex1[1];
            if(dy ==  0) {
                continue;
            }

            // Determine the highest and lowest positions of this edge
            int startY = Math.max((int)(vertex1[1]) + 1, 0);
            int endY = Math.min((int)(vertex2[1]), screen_h-1);

            // Update the overall highest and lowest positions of the scan lines
            if(startY < scanUpperPosition )
                scanUpperPosition = startY;

            if(endY > scanLowerPosition)
                scanLowerPosition = endY;


            // Calculate the gradient of the x value change of the edge
            float gradient = (vertex2[0] - vertex1[0]) /dy;

            // Calculate the gradient of the depth change of the edge
            float dz_y = (depth2-depth1)/dy;

            // Use linear interpolation to calculate the initial x value of the high position of this edge
            float startX = ((vertex1[0]) +  (startY - vertex1[1]) * gradient);

            // If the x value of the vertex is less than 0, it is considered that the triangle is tangent to the left side of the screen
            if(startX < 0 ) {
                isClippingRightOrLeft = true;
            }

            float tempZ= depth1  - vertex1[1]*dz_y + startY*dz_y;
            for (int y=startY; y<=endY; y++, startX+=gradient, tempZ+=dz_y) {
                // Store the x value of the descending edge on the right side of the scan line, otherwise, put it on the left side
                if(downwards){
                    xRight[y] = (int)startX;
                    zRight[y] = tempZ;
                }else{
                    xLeft[y] = (int)startX;
                    zLeft[y] = tempZ;
                }
            }
        }

        // When the triangle is tangent to both sides of the screen, we need to modify the two edges of the scan line arrays to ensure their values are between 0 and screen_w
        if(isClippingRightOrLeft) {
            int x_left, x_right;
            boolean xLeftInView, xRightInView;
            for(int y = scanUpperPosition; y <= scanLowerPosition; y++){   // Process each scan line row by row
                x_left = xLeft[y];
                x_right = xRight[y];

                xLeftInView = x_left >=0 && x_left < screen_w;       // Is the left edge within the screen
                xRightInView = x_right >0 && x_right < screen_w;     // Is the right edge within the screen

                if(xLeftInView && xRightInView)        // If both are within the screen, do nothing
                    continue;

                if(x_left >= screen_w  || x_right <= 0){   // If there are illogical left or right edges, set the length of this scan line to 0 to avoid rendering
                    xLeft[y] = 0;
                    xRight[y] = 0;
                    continue;
                }

                float dx =  x_right - x_left;          // Calculate the length of the scan line
                float dz = zRight[y] - zLeft[y];       // Calculate the depth difference between the left and right edges of the scan line

                if(!xLeftInView){           // If the left edge is to the left of the screen's left side
                    xLeft[y] = 0;           // Set the left edge at the screen's leftmost end
                    zLeft[y] = (zLeft[y] + dz /dx * (0 - x_left) ) ;  // Use linear interpolation to calculate the depth value of the left edge

                }

                if(!xRightInView){          // If the right edge is to the right of the screen's right side
                    xRight[y] = screen_w;   // Set the right edge at the screen's rightmost end
                    zRight[y] = (zRight[y] - dz /dx * (x_right - screen_w));   // Use linear interpolation to calculate the depth value of the right edge
                }
            }
        }
    }


    // Coloring the pixels of a triangle
    public static void renderTriangle() {
        // Choose different rendering methods based on the category of triangles
        if(renderType == 0)
            rendersolidTriangle();

    }

    // Draw a monochromatic triangle
    public static void rendersolidTriangle() {
        // Line by line rendering of scan lines
        for(int i = scanUpperPosition; i <= scanLowerPosition; i++){
            int x_left = xLeft[i] ;
            int x_right = xRight[i];

            float z_Left = zLeft[i];
            float z_Right = zRight[i];

            float dz = (z_Right- z_Left)/(x_right - x_left);

            x_left+=i * screen_w;
            x_right+=i * screen_w;

            for(int j = x_left;  j < x_right; j++, z_Left+=dz){
                if(zBuffer[j] < z_Left) {
                    zBuffer[j] = z_Left;
                    screen[j] = triangleColor;
                }
            }
        }
    }
}
