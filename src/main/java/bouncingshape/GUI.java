package bouncingshape;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class GUI extends JFrame {
    private JTextField imagePathField;
    private JTextField speedField;
    private JTextField directionXField;
    private JTextField directionYField;
    private JTextField directionZField;
    private JButton submitButton;

    public GUI() {
        super("Command Line Arguments Input");
        createUI();
    }

    private void createUI() {
        setLayout(new BorderLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 200);
        setLocationRelativeTo(null);

        JPanel panel = new JPanel(new GridLayout(6, 2));

        panel.add(new JLabel("Image Path:"));
        imagePathField = new JTextField(20);
        panel.add(imagePathField);

        panel.add(new JLabel("Speed:"));
        speedField = new JTextField(20);
        panel.add(speedField);

        panel.add(new JLabel("Direction X:"));
        directionXField = new JTextField(10);
        panel.add(directionXField);

        panel.add(new JLabel("Direction Y:"));
        directionYField = new JTextField(10);
        panel.add(directionYField);

        panel.add(new JLabel("Direction Z:"));
        directionZField = new JTextField(10);
        panel.add(directionZField);

        submitButton = new JButton("Submit");
        submitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                submitAction();
            }
        });

        panel.add(submitButton);
        add(panel, BorderLayout.CENTER);

        setVisible(true);
    }

    private void submitAction() {
        String imagePath = imagePathField.getText();
        float speed = Float.parseFloat(speedField.getText());
        float[] direction = new float[3];
        direction[0] = Float.parseFloat(directionXField.getText());
        direction[1] = Float.parseFloat(directionYField.getText());
        direction[2] = Float.parseFloat(directionZField.getText());

        if (imagePath == null || imagePath.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Model path not provided.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        new MainThread(imagePath, speed, direction);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new GUI();
            }
        });
    }
}
