package bouncingshape;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferInt;
import java.io.IOException;
import java.util.Arrays;

public class MainThread extends JFrame implements KeyListener {
	public static int screen_w = 1024;
	public static int screen_h = 682;
	public static int half_screen_w = screen_w/2;
	public static int half_screen_h = screen_h/2;
	public static int screenSize = screen_w * screen_h;

	// Use JPanel as the canvas
	public static JPanel panel;

	// Use an int array to store pixel values on the screen
	public static int[] screen;

	// Use a float array to store depth buffer values of the screen
	public static float[] zBuffer;

	// Screen image buffer. It provides methods for manipulating images on the screen in memory
	public static BufferedImage screenBuffer;

	// Record the number of frames rendered
	public static int frameIndex;

	// The interval time between each frame (milliseconds)
	public static int frameInterval = 32;

	// CPU sleep time, the smaller the number, the higher the computational efficiency
	public static int sleepTime, averageSleepTime;

	// Frame rate, and some auxiliary parameters for calculating the frame rate
	public static int framePerSecond;
	public static long lastDraw;

	// Total number of rendered triangles
	public static int triangleCount;

	private VertexBufferObject vbo;

	private VertexBufferObject borderVBO;
	private VertexBufferObject obstaclesVBO;

	private float border;

	public static void main(String[] args) {
		System.out.println("Command line arguments:");
		for (String arg : args) {
			System.out.println(arg);
		}

		String imagePath = null;
		float speed = 0f;
		float[] direction = new float[3]; // Use an array instead of Vector3D

		// Parse command line arguments
		imagePath=args[2];
		speed=Float.parseFloat(args[4]);
		direction[0] = Float.parseFloat(args[6].replace(",", ""));
		direction[1] = Float.parseFloat(args[7].replace(",", ""));
		direction[2] = Float.parseFloat(args[8]);

		if (imagePath == null || imagePath.isEmpty()) {
			System.err.println("Model path not provided or invalid.");
			System.exit(1); // Exit the program
		}

		// Create an instance of MainThread and start the rendering process
		new MainThread(imagePath, speed, direction);
	}

	public MainThread(String imagePath, float speed, float[] direction) {
		// Initialize the window
		initializeWindow();

		// Load the model
		loadModel(imagePath);

		float initialBorderSize =50f;
		this.border=initialBorderSize;
		initializeBorder(initialBorderSize);

		initializeObstacles(1,100);

		PhysicsEngine physicsEngine=new PhysicsEngine(this.obstaclesVBO,this.vbo,initialBorderSize,speed,direction);

		Rasterizer.init();

		// Enter the rendering loop
		renderLoop(physicsEngine);
	}

	private void initializeBorder(float l) {
		// Update vertex coordinates to reflect the new side length
		l=l;
		Vector3D[] vertices = {
				new Vector3D(-l, -l, -l), // Bottom left back
				new Vector3D(l, -l, -l),  // Bottom right back
				new Vector3D(l, -l, l),   // Bottom right front
				new Vector3D(-l, -l, l)   // Bottom left front
		};

		// Only include indices of the bottom face, with vertices defined clockwise (when viewed from above)
		int[] indices = {
				// Bottom face (with the front side facing up)
				3, 0, 1, 1, 2, 3
		};

		// Create and assign borderVBO
		this.borderVBO = new VertexBufferObject(vertices, indices);
	}

	private void initializeObstacles(float size, int obstacleCount) {
		// Create an array to hold all vertices and indices of the obstacles
		Vector3D[] allVertices = new Vector3D[8 * obstacleCount];
		int[] allIndices = new int[36 * obstacleCount];

		// Loop to generate and position each obstacle
		for (int i = 0; i < obstacleCount; i++) {
			// Calculate a random position for the obstacle within a certain range
			float x = (float) (Math.random() * 2*border + -border);
			float y = (float) (Math.random() * 2*border + -border);
			float z = (float) (Math.random()* 2*border + -border);

			// Generate vertices for the obstacle
			Vector3D[] obstacleVertices = {
					new Vector3D(-size + x, -size + y, -size + z), // Bottom left back
					new Vector3D(size + x, -size + y, -size + z),  // Bottom right back
					new Vector3D(size + x, -size + y, size + z),   // Bottom right front
					new Vector3D(-size + x, -size + y, size + z),  // Bottom left front
					new Vector3D(-size + x, size + y, -size + z),  // Top left back
					new Vector3D(size + x, size + y, -size + z),   // Top right back
					new Vector3D(size + x, size + y, size + z),    // Top right front
					new Vector3D(-size + x, size + y, size + z)    // Top left front
			};

			// Calculate the starting index for this obstacle in the allVertices array
			int startIndex = i * 8;

			// Copy the obstacle vertices into the allVertices array
			System.arraycopy(obstacleVertices, 0, allVertices, startIndex, obstacleVertices.length);

			// Calculate the starting index for this obstacle in the allIndices array
			int startIndices = i * 36;

			// Generate indices for the obstacle (using triangles)
			int[] obstacleIndices = {
					// Bottom face
					startIndex + 3, startIndex, startIndex + 1,
					startIndex + 1, startIndex + 2, startIndex + 3,
					// Top face
					startIndex + 5, startIndex + 4, startIndex + 7,
					startIndex + 7, startIndex + 6, startIndex + 5,
					// Front face
					startIndex + 3, startIndex + 2, startIndex + 6,
					startIndex + 6, startIndex + 7, startIndex + 3,
					// Back face
					startIndex + 1, startIndex, startIndex + 4,
					startIndex + 4, startIndex + 5, startIndex + 1,
					// Left face
					startIndex, startIndex + 3, startIndex + 7,
					startIndex + 7, startIndex + 4, startIndex,
					// Right face
					startIndex + 2, startIndex + 1, startIndex + 5,
					startIndex + 5, startIndex + 6, startIndex + 2
			};

			// Copy the obstacle indices into the allIndices array
			System.arraycopy(obstacleIndices, 0, allIndices, startIndices, obstacleIndices.length);
		}

		// Create and assign obstaclesVBO
		this.obstaclesVBO = new VertexBufferObject(allVertices, allIndices);
	}


	private void initializeWindow() {
		setTitle("Rendering");
		panel= (JPanel) this.getContentPane();
		panel.setPreferredSize(new Dimension(screen_w, screen_h));
		panel.setMinimumSize(new Dimension(screen_w,screen_h));
		panel.setLayout(null);

		setResizable(false);
		pack();
		setVisible(true);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


		screenBuffer =  new BufferedImage(screen_w, screen_h, BufferedImage.TYPE_INT_RGB);
		DataBuffer dest = screenBuffer.getRaster().getDataBuffer();
		screen = ((DataBufferInt)dest).getData();

		// Initialize the depth buffer
		zBuffer = new float[screenSize];

		// Initialize lookup tables
		LookupTables.init();

		// Initialize the rasterizer
		Rasterizer.init();

		// Initialize the camera view
		Camera.init(0,-40,-100);

		// Add key listener
		addKeyListener(this);
	}

	private void loadModel(String modelPath) {
		try {
			this.vbo = MeshLoader.loadMeshFromFile(modelPath);
			System.out.println("Model loaded successfully from: " + modelPath);
			System.out.println(this.vbo.vertexCount);
		} catch (IOException e) {
			System.err.println("Failed to load model: " + e.getMessage());
			e.printStackTrace();
			System.exit(1); // Exit the program in case of failure
		}
	}

	private void renderLoop(PhysicsEngine physicsEngine) {
		long lastFrameTime = System.nanoTime();
		while (true) {
			triangleCount = 0;
			long startTime = System.currentTimeMillis();
			long currentTime = System.nanoTime();
			float deltaTime = (currentTime - lastFrameTime) / 1000000000.0f;
			lastFrameTime = currentTime;

			// Update the camera
			Camera.update();

			// Clear the screen and depth buffer
			clearScreen();
			zBuffer[0] = 0;
			Arrays.fill(zBuffer, 0);

			// Update physics state
			physicsEngine.update(deltaTime);


			Rasterizer.VBO = this.vbo;

			// Perform the rendering
			// Render the torus 1
			Rasterizer.triangleColor = 0xCD5C5C;
			Rasterizer.localTranslation.set(0, 0, 0);
			Rasterizer.renderType = 0;
			Rasterizer.rasterize();

			if (borderVBO != null) {
				Rasterizer.triangleColor = 0x008B8B;
				Rasterizer.VBO = borderVBO;
				Rasterizer.rasterize();
			}
			if (obstaclesVBO != null) {
				Rasterizer.triangleColor = 0xFFA500;
				Rasterizer.VBO = obstaclesVBO;
				Rasterizer.rasterize();
			}

			// Draw the render result to the screen
			drawToScreen();
			frameIndex++;

			// Control the frame rate
			controlFrameRate(startTime);

			// Update the window title, e.g., with FPS
			updateWindowTitle();
		}
	}



	private void clearScreen() {

		Arrays.fill(screen, 0, screenSize, (163 << 16) | (216 << 8) | 239);
	}

	private void drawToScreen() {
//		System.out.println("Drawing to screen...");
		Graphics g = panel.getGraphics();
		g.drawImage(screenBuffer, 0, 0, this);
	}




	private void controlFrameRate(long startTime) {
		long frameTime = System.currentTimeMillis() - startTime;
		long sleepTime = frameInterval - frameTime;
		if (sleepTime > 0) {
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}


	private void updateWindowTitle() {
		long currentTime = System.currentTimeMillis();
		if (currentTime - lastDraw >= 1000) {
			framePerSecond = frameIndex;
			frameIndex = 0;
			lastDraw = currentTime;
			this.setTitle(String.format("FPS: %d | Triangles: %d", framePerSecond, this.vbo.triangleCount));
		}
	}


	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyChar() == 'w' || e.getKeyChar() == 'W')
			Camera.MOVE_FORWARD = true;
		else if(e.getKeyChar() == 's' || e.getKeyChar() == 'S')
			Camera.MOVE_BACKWARD = true;
		else if(e.getKeyChar() == 'a' || e.getKeyChar() == 'A')
			Camera.SLIDE_LEFT = true;
		else if(e.getKeyChar() == 'd' || e.getKeyChar() == 'D')
			Camera.SLIDE_RIGHT = true;


		if(e.getKeyCode() == KeyEvent.VK_UP)
			Camera.LOOK_UP= true;
		else if(e.getKeyCode() == KeyEvent.VK_DOWN)
			Camera.LOOK_DOWN = true;
		else if(e.getKeyCode() == KeyEvent.VK_LEFT)
			Camera.LOOK_LEFT = true;
		else if(e.getKeyCode() == KeyEvent.VK_RIGHT)
			Camera.LOOK_RIGHT = true;

	}

	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getKeyChar() == 'w' || e.getKeyChar() == 'W')
			Camera.MOVE_FORWARD = false;
		else if(e.getKeyChar() == 's' || e.getKeyChar() == 'S')
			Camera.MOVE_BACKWARD = false;
		else if(e.getKeyChar() == 'a' || e.getKeyChar() == 'A')
			Camera.SLIDE_LEFT = false;
		else if(e.getKeyChar() == 'd' || e.getKeyChar() == 'D')
			Camera.SLIDE_RIGHT = false;

		if(e.getKeyCode() == KeyEvent.VK_UP)
			Camera.LOOK_UP= false;
		else if(e.getKeyCode() == KeyEvent.VK_DOWN)
			Camera.LOOK_DOWN = false;
		else if(e.getKeyCode() == KeyEvent.VK_LEFT)
			Camera.LOOK_LEFT = false;
		else if(e.getKeyCode() == KeyEvent.VK_RIGHT)
			Camera.LOOK_RIGHT = false;

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}


}

