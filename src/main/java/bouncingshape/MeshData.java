package bouncingshape;

public class MeshData {
    private VertexBufferObject vbo;
    private float speed;
    private Vector3D direction;

    public MeshData(VertexBufferObject vbo, float speed, Vector3D direction) {
        this.vbo = vbo;
        this.speed = speed;
        this.direction = direction;
    }

    // Getters
    public VertexBufferObject getVbo() {
        return vbo;
    }

    public float getSpeed() {
        return speed;
    }

    public Vector3D getDirection() {
        return direction;
    }
}

