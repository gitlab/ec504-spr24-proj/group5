package bouncingshape;
import java.util.Objects;
public class Vector3D {

	public float x, y, z;
	

    public Vector3D(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    

    public void set(Vector3D v) {
        this.x=v.x;
        this.y=v.y;
        this.z=v.z;
    }
    
    public void set(float x , float y, float z) {
        this.x=x;
        this.y=y;
        this.z=z;
    }
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		Vector3D vector3D = (Vector3D) obj;
		return Float.compare(vector3D.x, x) == 0 &&
				Float.compare(vector3D.y, y) == 0 &&
				Float.compare(vector3D.z, z) == 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y, z);
	}

    public void add(Vector3D v) {
        this.x+=v.x;
        this.y+=v.y;
        this.z+=v.z;
    }
    
    public void add(float x, float y, float z) {
        this.x+=x;
        this.y+=y;
        this.z+=z;
    }
    
    public void add(Vector3D v, float scaler){
    	this.x += v.x * scaler;
		this.y += v.y * scaler;
		this.z += v.z * scaler;
    }
    

    public void subtract(Vector3D v) {
    	 this.x-=v.x;
         this.y-=v.y;
         this.z-=v.z;
    }
    
    public void subtract(Vector3D v, float scaler){
    	x -= v.x * scaler;
    	y -= v.y * scaler;
    	z -= v.z * scaler;
    }
    

    public float dot(Vector3D v2){
		return this.x*v2.x + this.y*v2.y + this.z*v2.z;
	}
    
    public float dot(float x, float y, float z){
		return this.x*x + this.y*y + this.z*z;
	}
    

    public void cross(Vector3D v1, Vector3D v2){
		x = v1.y*v2.z - v1.z*v2.y;
		y = v1.z*v2.x - v1.x*v2.z;
		z = v1.x*v2.y - v1.y*v2.x;
	}
    
	public  Vector3D cross(Vector3D v){
		return new Vector3D(y*v.z - z*v.y, z*v.x - x*v.z, x*v.y - y*v.x);
	}
    

    public float getLength() {
        return (float) Math.sqrt(x * x + y * y + z * z);
    }
    

	public void unit(){
		float length = getLength();
		x = x/length;
		y = y/length;
		z = z/length;
	}
	

    public void scale(float scalar) {
    	x*=scalar;
		y*=scalar;
		z*=scalar;
    }
    

  	public void  rotate_Y(int angle){
  		float sin = LookupTables.sin[angle];
  		float cos = LookupTables.cos[angle];
  		float old_X = x;
  		float old_Z = z;
  		x = cos*old_X + sin*old_Z;
  		z = - sin*old_X + cos*old_Z;
  	}
  	
  	public void  rotate_Y(float sin, float cos){
  		float old_X = x;
  		float old_Z = z;
  		x = cos*old_X + sin*old_Z;
  		z = -sin*old_X + cos*old_Z;
  	}


  	public void rotate_X(int angle){
  		float sin = LookupTables.sin[angle];
  		float cos = LookupTables.cos[angle];
  		float old_Y = y;
  		float old_Z = z;
  		y = cos*old_Y + sin*old_Z;
  		z = -sin*old_Y + cos*old_Z;
  	}
  	
	public void rotate_X(float sin, float cos){
  		float old_Y = y;
  		float old_Z = z;
  		y = cos*old_Y + sin*old_Z;
  		z = -sin*old_Y + cos*old_Z;
  	}
  	

  	public void rotate_Z(int angle){
  		float sin = LookupTables.sin[angle];
  		float cos = LookupTables.cos[angle];
  		float old_X = x;
  		float old_Y = y;
  		x = cos*old_X + sin*old_Y;
  		y = -sin*old_X + cos*old_Y;
  	}
  	
	public void rotate_Z(float sin, float cos){
  		float old_X = x;
  		float old_Y = y;
  		x = cos*old_X + sin*old_Y;
  		y = -sin*old_X + cos*old_Y;
  	}
    

    public String toString() {
    	return "(" + x + ", " + y + ", " + z + ")";
    }
}
