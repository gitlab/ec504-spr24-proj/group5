package bouncingshape;

import com.bulletphysics.collision.narrowphase.ManifoldPoint;
import com.bulletphysics.dynamics.RigidBody;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.collision.shapes.HullCollisionShape;
import com.jme3.bullet.objects.PhysicsRigidBody;
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.VertexBuffer.Type;
import com.jme3.util.BufferUtils;

import java.util.Arrays;

public class PhysicsEngine implements PhysicsCollisionListener {

    private float gravity=-10f;
    private float influenceRadius=1f;
    private float deformationStrength=1f;

    private float proportionalCoefficient=100f;
    private float inverseProportionalCoefficient=0.5f;
    private PhysicsSpace physicsSpace;
    private Mesh mesh;
    private VertexBufferObject vbo;

    private VertexBufferObject initialVBO;

    private Vector3f initialVelocity;

    private VertexBufferObject obstaclesVBO;



    public PhysicsEngine ( VertexBufferObject obstaclesVBO, VertexBufferObject vbo, float size, float initialSpeed, float[] initialSpeedDirection) {
        BulletAppState bulletAppState = new BulletAppState();
        bulletAppState.startPhysics();
        physicsSpace = bulletAppState.getPhysicsSpace();


        this.mesh = createMeshFromVBO(vbo.vertexBuffer, vbo.indexBuffer);
        this.vbo=vbo;
        this.initialVBO = deepCopyVBO(vbo);
        this.obstaclesVBO=obstaclesVBO;
        addObstaclesToPhysicsSpace();
        this.initialVelocity = new Vector3f(initialSpeedDirection[0], initialSpeedDirection[1], initialSpeedDirection[2]).mult(initialSpeed);


        initPhysicsWorld(size);

        addMeshToPhysicsSpace(mesh, 1.0f, initialSpeedDirection);
    }

    private void addObstaclesToPhysicsSpace() {
        Mesh obstacleMesh = createMeshFromVBO(obstaclesVBO.vertexBuffer, obstaclesVBO.indexBuffer);
        Geometry obstacleGeometry = new Geometry("Obstacle", obstacleMesh);
        RigidBodyControl obstacleControl = new RigidBodyControl(0);
        obstacleGeometry.addControl(obstacleControl);
        physicsSpace.add(obstacleGeometry);
        physicsSpace.add(obstacleControl);
    }
    private VertexBufferObject deepCopyVBO(VertexBufferObject source) {
        Vector3D[] sourceVertices = source.vertexBuffer;
        int[] sourceIndices = source.indexBuffer;
        Vector3D[] copiedVertices = new Vector3D[sourceVertices.length];
        int[] copiedIndices = Arrays.copyOf(sourceIndices, sourceIndices.length);

        for (int i = 0; i < sourceVertices.length; i++) {
            copiedVertices[i] = new Vector3D(sourceVertices[i].x, sourceVertices[i].y, sourceVertices[i].z);
        }

        return new VertexBufferObject(copiedVertices, copiedIndices);
    }

    private Mesh createMeshFromVBO(Vector3D[] vertices, int[] indices) {
        Mesh mesh = new Mesh();

        Vector3f[] jmeVertices = new Vector3f[vertices.length];
        for (int i = 0; i < vertices.length; i++) {
            jmeVertices[i] = new Vector3f(vertices[i].x, vertices[i].y, vertices[i].z);
        }

        mesh.setBuffer(Type.Position, 3, BufferUtils.createFloatBuffer(jmeVertices));
        mesh.setBuffer(Type.Index, 3, BufferUtils.createIntBuffer(indices));
        mesh.updateBound();
        mesh.updateCounts();

        return mesh;
    }

    private void initPhysicsWorld(float size) {
        physicsSpace.setGravity(new Vector3f(0, this.gravity, 0));
        addBoxBoundaries(size);

    }


    private void addBoxBoundaries(float size) {
        float thickness = 0.05f;

        // Bottom face
        BoxCollisionShape bottomShape = new BoxCollisionShape(new Vector3f(size, thickness, size));
        PhysicsRigidBody bottomBoundary = new PhysicsRigidBody(bottomShape, 0);
        bottomBoundary.setPhysicsLocation(new Vector3f(0, -size - thickness, 0));
        physicsSpace.add(bottomBoundary);

        // Top face
        PhysicsRigidBody topBoundary = new PhysicsRigidBody(bottomShape, 0);
        topBoundary.setPhysicsLocation(new Vector3f(0, size + thickness, 0));
        physicsSpace.add(topBoundary);

        // Front face
        BoxCollisionShape frontShape = new BoxCollisionShape(new Vector3f(size, size, thickness));
        PhysicsRigidBody frontBoundary = new PhysicsRigidBody(frontShape, 0);
        frontBoundary.setPhysicsLocation(new Vector3f(0, 0, -size - thickness));
        physicsSpace.add(frontBoundary);

        // Back face
        PhysicsRigidBody backBoundary = new PhysicsRigidBody(frontShape, 0);
        backBoundary.setPhysicsLocation(new Vector3f(0, 0, size + thickness));
        physicsSpace.add(backBoundary);

        // Left face
        BoxCollisionShape leftShape = new BoxCollisionShape(new Vector3f(thickness, size, size));
        PhysicsRigidBody leftBoundary = new PhysicsRigidBody(leftShape, 0);
        leftBoundary.setPhysicsLocation(new Vector3f(-size - thickness, 0, 0));
        physicsSpace.add(leftBoundary);

        // Right face
        PhysicsRigidBody rightBoundary = new PhysicsRigidBody(leftShape, 0);
        rightBoundary.setPhysicsLocation(new Vector3f(size + thickness, 0, 0));
        physicsSpace.add(rightBoundary);
    }





    private void addMeshToPhysicsSpace(Mesh mesh, float mass, float[] initialSpeedDirection) {
        HullCollisionShape hullShape = new HullCollisionShape(mesh);
        RigidBodyControl bodyControl = new RigidBodyControl(hullShape, mass);
        bodyControl.setUserObject(vbo);
        bodyControl.setLinearVelocity(initialVelocity);

        physicsSpace.add(bodyControl);
    }


    public void update(float deltaTime) {
        physicsSpace.update(deltaTime);

        for (PhysicsRigidBody control : physicsSpace.getRigidBodyList()) {
            if (control.getUserObject() instanceof VertexBufferObject) {
                VertexBufferObject vbo = (VertexBufferObject) control.getUserObject();
                updateVBOFromPhysics(vbo, (RigidBodyControl) control, deltaTime);
            }
        }
    }

    private void updateVBOFromPhysics(VertexBufferObject vbo, RigidBodyControl control, float deltaTime) {
        Vector3f location = control.getPhysicsLocation();
        Quaternion rotation = control.getPhysicsRotation();
        Vector3f velocity = control.getLinearVelocity();


        Vector3f displacement = new Vector3f(velocity).mult(deltaTime);

        for (int i = 0; i < initialVBO.vertexCount; i++) {
            Vector3f vertexPos = new Vector3f(initialVBO.vertexBuffer[i].x, initialVBO.vertexBuffer[i].y, initialVBO.vertexBuffer[i].z);


            vertexPos.addLocal(displacement);

            rotation.mult(vertexPos, vertexPos);
            vertexPos.addLocal(location);

            vbo.vertexBuffer[i].x = vertexPos.x;
            vbo.vertexBuffer[i].y = vertexPos.y;
            vbo.vertexBuffer[i].z = vertexPos.z;
        }
    }

    @Override
    public void collision(PhysicsCollisionEvent event) {
        if (event.getNodeA().getName().equals("Mesh") || event.getNodeB().getName().equals("Mesh")) {
            float collisionImpulse = event.getAppliedImpulse();
            Vector3f collisionNormal = event.getNormalWorldOnB();

            for (int i = 0; i < vbo.vertexCount; i++) {
                Vector3f vertex = new Vector3f(vbo.vertexBuffer[i].x, vbo.vertexBuffer[i].y, vbo.vertexBuffer[i].z);
                float distance = vertex.distance(collisionNormal.mult(collisionImpulse));

                if (distance < influenceRadius) {
                    float baseMoveAmount = (influenceRadius - distance) / influenceRadius * collisionImpulse * deformationStrength;
                    float adjustedMoveAmount = baseMoveAmount * (proportionalCoefficient - (distance * inverseProportionalCoefficient));

                    Vector3f displacement = collisionNormal.negate().multLocal(adjustedMoveAmount);
                    vertex.addLocal(displacement);

                    vbo.vertexBuffer[i].x = vertex.x;
                    vbo.vertexBuffer[i].y = vertex.y;
                    vbo.vertexBuffer[i].z = vertex.z;
                }
            }
        }
    }
}
