package bouncingshape;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MeshLoader {
    public static VertexBufferObject loadMeshFromFile(String filePath) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        String line;
        List<Vector3D> vertexList = new ArrayList<>();
        List<Integer> indexList = new ArrayList<>();
        Map<Vector3D, Integer> vertexMap = new HashMap<>();
        int index = 0;

        while ((line = reader.readLine()) != null) {
            String[] tokens = line.split("]\\s*\\[");
            for (String token : tokens) {
                String cleanedToken = token.replace("[", "").replace("]", "");
                String[] coords = cleanedToken.split(",");
                if (coords.length == 3) {
                    float x = Float.parseFloat(coords[0]);
                    float y = Float.parseFloat(coords[1]);
                    float z = Float.parseFloat(coords[2]);
                    Vector3D newVertex = new Vector3D(x, y, z);
                    if (!vertexMap.containsKey(newVertex)) {
                        vertexList.add(newVertex);
                        vertexMap.put(newVertex, index);
                        indexList.add(index);
                        index++;
                    } else {
                        indexList.add(vertexMap.get(newVertex));
                    }
                }
            }
        }
        reader.close();

        Vector3D[] vertices = vertexList.toArray(new Vector3D[0]);
        int[] indices = indexList.stream().mapToInt(i -> i).toArray();
        return new VertexBufferObject(vertices, indices);
    }

}
