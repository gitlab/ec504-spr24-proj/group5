package bouncingshape;

public class CommandLineParser {
    private String imagePath;
    private float initialSpeed;
    private float[] direction = new float[3]; // 存储方向向量的XYZ分量

    public CommandLineParser(String[] args) {
        for (int i = 0; i < args.length; i++) {
            switch (args[i]) {
                case "--image":
                    if (i + 1 < args.length) {
                        imagePath = args[i + 1];
                    }
                    break;
                case "--speed":
                    if (i + 1 < args.length) {
                        initialSpeed = Float.parseFloat(args[i + 1]);
                    }
                    break;
                case "--dir":
                    if (i + 1 < args.length) {
                        String[] dirComponents = args[i + 1].split(",");
                        if (dirComponents.length == 3) {
                            for (int j = 0; j < 3; j++) {
                                direction[j] = Float.parseFloat(dirComponents[j]);
                            }
                        }
                    }
                    break;
            }
        }
    }

    public String getImagePath() {
        return imagePath;
    }

    public float getInitialSpeed() {
        return initialSpeed;
    }

    public float[] getDirection() {
        return direction;
    }

    // 用于测试的main方法
    public static void main(String[] args) {

        String[] simulatedArgs = {
                "--image", "path/to/image/file",
                "--speed", "1.0",
                "--dir", "0.5,0.5,0.5"
        };

        CommandLineParser parser = new CommandLineParser(simulatedArgs);

        System.out.println("Image Path: " + parser.getImagePath());
        System.out.println("Initial Speed: " + parser.getInitialSpeed());
        System.out.printf("Direction: [%.2f, %.2f, %.2f]%n",
                parser.getDirection()[0],
                parser.getDirection()[1],
                parser.getDirection()[2]);
    }
}
