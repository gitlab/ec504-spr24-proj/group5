**Accepted feedback:**
1. Collision bug: We solved the problem that the original model can only bounce once and pass through the ground when it hits the ground (because this is a main function of this project currently, we need to solve it first)
2. Removed unnecessary comments (because this will affect students’ reading of our project code)
3. Our model will produce collision and slight deformation effects when it hits each boundary (because this is another main function of our project, so we need to implement it)



**Other changes (Add obstacles to make collisions more realistic)**
1. We Add 100 fixed (immovable) obstacles to the environment of the bouncing figure
2. Our model will have a blocking effect after encountering an obstacle, which will change the direction of the object and reduce the subsequent movement speed of the object.
