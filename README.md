# Group5

## Name
Bouncing Shape

## Description
This Project aim to Design and implement a Java application that efficiently bounces a complex, deformable, three-dimensional figure around a window. The figure should deform (somewhat) upon touching other surfaces, such as the sides of the window or any other figures, and the code must also report, at regular intervals, the frame rate (frames/second) that is being displayed on the screen. 

## Group members
Mohan Guo, Wenyuan Cui, Jiangjian Xie, Zhekai Zhu, Shanshang Zeng

## Work breakdown
Mohan Guo, Jiangjian Xie-- The rasterization part.

Wenyuan Cui -- Model main thread and the collision physical engine part.

Shanshang Zeng--The project leader, assisting and log writting.

Zhekai Zhu -- The deformation part.

## General implement
In this project, the VertexBufferObject (VBO) class is utilized to manage the geometric data of 3D models, facilitating the rendering and manipulation of complex figures. The jMonkeyEngine library is used to build physical simulation environment where it handles collisions of objects.  Each 3D model is represented by a RigidBody in jMonkeyEngine, allowing it to interact physically with other objects and forces in the simulation. A tunable deformation algorithm is designed. Changes from physical interactions, like collisions, are reflected back in the VBO, updating the model's geometry in real-time. Then this updated VBO will be used to rasterize so that the shape in the frame will update.

## Features

Minimum requirements:

1. Real-Time Physics Simulation：
jMonkeyEngine is employed to add real-time physical effects to games and interactive applications. The data in that outer file will be stored in a customized data structure VertexBufferObject, where the location of points and indices will be stored. This data structure can be used by jMonkeyEngine to form rigid body by converting the VBO into the jMonkeyEngine needed data type Mesh. This transition from vbo to mesh costs O(n) time.

2. Deformation:
Deformation is designed according to several factors. The details are stated in the slides. This is also an O(n) algorithm where all components in vbo that is close to the point where collision happens will be iterated.

3. Rasterization:
Rasterization part is completed following the commonly used rasterization procedure. 

More features:

1. Physical Engine:
jMonkeyEngine can provide the customized gravity. The elasticity of triangle edges is implemented in the deformation part.

2. Add obstacles:
100 imovable cube obstacles are added.


## Usage
Unzip the file. This project is based on gradle. You have to install gradle here:https://gradle.org/.

After downloading and build gradle, you can run the "group5\src\main\java\bouncingshape\GUI.java". The image files are also in the "group5\src\main\java\bouncingshape". For example "src/main/java/bouncingshape/sphere_triangles.txt". Speed and directions are the initial attributes of the object.

The program some times will crash and error "Process 'command 'C:\Program Files\Java\jdk\bin\java.exe'' finished with non-zero exit value 1 eclipse". You can try "File -> invalidate cache.. -> invalidate and restart" if you are using Intellij. This problem also sometimes exists when the input image file is too large.

## Code
 Link to the branch containing all complete: https://agile.bu.edu/gitlab/ec504/ec504_projects/group5.git
